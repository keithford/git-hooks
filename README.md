# git hooks

This is a collection of git hooks. Some I created, some I found on the internet.

## Getting started

If you are not familiar with git hooks, you may want to do some
reading. This is a good place to start: https://githooks.com/

Hooks are placed in the `.git/hooks` directory of your project.
If you already have a hook in place, then check the internet to 
find ways to use multiple hooks.

## pre-commit-forgotten-debug

This hook will trigger before a commit. It will search the diff
of the changes for a set of search strings. You will want to edit
these for your needs.
